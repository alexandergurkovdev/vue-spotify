import Vue from 'vue';
import App from './App.vue';

import store from './store';
import router from './router';

import VueCookies from 'vue-cookies';
import vueSlider from 'vue-slider-component';
import DropdownMenu from '@innologica/vue-dropdown-menu';
import VueLazyload from 'vue-lazyload';
import VueMoment from 'vue-moment';
import Toasted from 'vue-toasted';
import VueTippy, { TippyComponent } from 'vue-tippy';
import firebase from 'firebase/app';

import './directives';
import 'optiscroll';

import 'vue-slider-component/theme/default.css';

import './filters';

Vue.use(VueCookies);
Vue.use(VueLazyload);
Vue.use(VueMoment);
Vue.use(Toasted, {
  theme: 'toasted-primary',
  position: 'bottom-center',
  duration: 5000
});
Vue.use(VueTippy);
Vue.component('tippy', TippyComponent);

Vue.component('vueSlider', vueSlider);
Vue.component('DropdownMenu', DropdownMenu);

Vue.config.productionTip = false;

Vue.$cookies.config('1d');

const spotifyToken = Vue.$cookies.get('spotifyToken');

if (!spotifyToken) {
  store.commit('auth/setToken', null);
} else {
  store.commit('auth/setToken', spotifyToken);
}

const firebaseConfig = {
  apiKey: 'AIzaSyB_EBCCRCK-1xGT6Zb7Hi0wsyUjmCwFtFw',
  authDomain: 'vue-spotify.firebaseapp.com',
  projectId: 'vue-spotify',
  storageBucket: 'vue-spotify.appspot.com',
  messagingSenderId: '722187917988',
  appId: '1:722187917988:web:aff75decd46625efd22431'
};

firebase.initializeApp(firebaseConfig);
window.firebase = firebase;

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
