import Vue from 'vue';
import VueRouter from 'vue-router';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Home',
    meta: { requireAuth: true, layout: 'main' },
    redirect: {
      name: 'browse'
    }
  },
  {
    path: '/login',
    name: 'login',
    meta: { layout: 'auth' },
    component: () => import(/* webpackChunkName: "login" */ '../views/Login.vue')
  },
  {
    path: '/browse',
    name: 'browse',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "browse" */ '../views/browse/Index.vue'),
    redirect: {
      name: 'browseGenres'
    },
    children: [
      {
        path: '/genres',
        meta: { requireAuth: true, layout: 'main' },
        name: 'browseGenres',
        component: () => import(/* webpackChunkName: "genres" */ '../views/browse/BrowseGenres.vue')
      },
      {
        path: '/newreleases',
        meta: { requireAuth: true, layout: 'main' },
        name: 'browseNewReleases',
        component: () => import(/* webpackChunkName: "newreleases" */ '../views/browse/BrowseNewReleases.vue')
      },
      {
        path: '/top-artists',
        meta: { requireAuth: true, layout: 'main' },
        name: 'browseTopArtists',
        component: () => import(/* webpackChunkName: "topArtists" */ '../views/browse/BrowseTopArtists.vue')
      }
    ]
  },
  {
    path: '/album/:id',
    name: 'album',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "album" */ '../views/album/')
  },
  {
    path: '/artist/:id',
    name: 'artist',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "artist" */ '../views/artist/')
  },
  {
    path: '/user/:user_id/playlist/:playlist_id',
    name: 'playlist',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "playlist" */ '../views/playlist/')
  },
  {
    path: '/collection/tracks',
    name: 'tracks-collection',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "tracksCollection" */ '../views/collection/Tracks.vue')
  },
  {
    path: '/collection/albums',
    name: 'albums-collection',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "albumsCollection" */ '../views/collection/Albums.vue')
  },
  {
    path: '/collection/artists',
    name: 'artists-collection',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "artistCollection" */ '../views/collection/Artists.vue')
  },
  {
    path: '/genres/:id',
    name: 'genres',
    meta: { requireAuth: true, layout: 'main' },
    component: () => import(/* webpackChunkName: "genres" */ '../views/genres/')
  },
  {
    path: '/search',
    name: 'search',
    component: () => import(/* webpackChunkName: "search" */ '../views/search/'),
    meta: { requireAuth: true, layout: 'main' },
    children: [
      // {
      //   path: '/result/:query',
      //   name: 'search-result',
      //   meta: { requireAuth: true, layout: 'main' },
      //   component: () => import(/* webpackChunkName: "searchResult" */ '../views/search/SearchResult.vue')
      // },
      {
        path: '/album/:query',
        name: 'search-album',
        meta: { requireAuth: true, layout: 'main' },
        component: () => import(/* webpackChunkName: "searchAlbums" */ '../views/search/SearchAlbums.vue')
      },
      {
        path: '/artist/:query',
        name: 'search-artist',
        meta: { requireAuth: true, layout: 'main' },
        component: () => import(/* webpackChunkName: "searchArtist" */ '../views/search/SearchArtist.vue')
      },
      {
        path: '/playlist/:query',
        name: 'search-playlist',
        meta: { requireAuth: true, layout: 'main' },
        component: () => import(/* webpackChunkName: "searchPlaylist" */ '../views/search/SearchPlaylist.vue')
      },
      {
        path: '/track/:query',
        name: 'search-track',
        meta: { requireAuth: true, layout: 'main' },
        component: () => import(/* webpackChunkName: "searchTrack" */ '../views/search/SearchTrack.vue')
      }
    ]
  },
  {
    path: '*',
    meta: { layout: 'main' },
    component: () => import(/* webpackChunkName: "notFound" */ '../views/NotFound.vue')
  }
];

const router = new VueRouter({
  mode: 'history',
  routes
});

router.beforeEach((to, from, next) => {
  const requireAuth = to.matched.some(record => record.meta.requireAuth);
  const token = store.state.auth.token;

  if (requireAuth && !token) {
    next('/login');
  } else if (to.matched.some(record => record.name === 'Login') && token) {
    next('/');
  } else {
    next();
  }
});

export default router;
