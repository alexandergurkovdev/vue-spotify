import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import player from './modules/player';
import profile from './modules/profile';
import albums from './modules/albums';
import playlist from './modules/playlist';
import tracks from './modules/tracks';
import newReleases from './modules/newReleases';
import genres from './modules/genres';
import artist from './modules/artist';
import myArtists from './modules/myArtists';
import myTopArtists from './modules/myTopArtists';
import search from './modules/search';

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    player,
    profile,
    albums,
    playlist,
    tracks,
    newReleases,
    genres,
    artist,
    myArtists,
    myTopArtists,
    search
  }
});
