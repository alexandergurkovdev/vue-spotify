import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    tracks: {
      offset: 0,
      limit: 20,
      total: 1,
      items: [],
      isMore: false
    },
    lastSavedTrack: '',
    lastRemovedTrack: ''
  },
  actions: {
    async getMyTracks({ commit, getters, rootState }) {
      if (getters.tracks.total > getters.tracks.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);
        await spotifyApi.getMySavedTracks({ offset: getters.tracks.offset, limit: getters.tracks.limit }).then(
          data => {
            commit('setOffset', data.offset + getters.tracks.limit);
            commit('setTotal', data.total);
            let items = [...data.items];
            items.forEach(item => {
              commit('setItems', item);
            });
            commit('setIsMore', false);
          },
          err => {
            Vue.toasted.show(`${err.error.message}`, {
              type: 'error'
            });
          }
        );
      }
    },
    async getMyChandedTracks({ state, rootState }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getMySavedTracks().then(
        data => {
          state.tracks.items = [...data.items];
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setItems(state, payload) {
      state.tracks.items = [...state.tracks.items, payload];
    },
    setOffset(state, payload) {
      state.tracks.offset = payload;
    },
    setTotal(state, payload) {
      state.tracks.total = payload;
    },
    setIsMore(state, payload) {
      state.tracks.isMore = payload;
    },
    setSavedTrack(state, payload) {
      state.lastSavedTrack = payload;
      state.lastRemovedTrack = '';
    },
    setRemovedTrack(state, payload) {
      state.lastRemovedTrack = payload;
      state.lastSavedTrack = '';
    }
  },
  getters: {
    tracks(state) {
      return state.tracks;
    },
    lastSavedTrack(state) {
      return state.lastSavedTrack;
    },
    lastRemovedTrack(state) {
      return state.lastRemovedTrack;
    }
  }
};
