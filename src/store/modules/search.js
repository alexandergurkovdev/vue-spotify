import api from '../../api';

export default {
  namespaced: true,
  state: {
    query: '',
    result: '',
    isLoading: false,
    error: null,
    albums: '',
    albumsIsLoading: false,
    albumsError: null,
    artists: '',
    artistsIsLoading: false,
    artistsError: null,
    playlists: '',
    playlistsIsLoading: false,
    playlistsError: null,
    tracks: '',
    tracksIsLoading: false,
    tracksError: null
  },
  actions: {
    async search({ commit }, query) {
      commit('requestSearch');
      commit('setSearchQuery', query);

      await api.search(query, ['album', 'artist', 'playlist', 'track']).then(
        data => {
          commit('requestSearchSuccess', data);
        },
        err => {
          commit('requestSearchError', err);
        }
      );
    },
    async getAlbums({ commit, state: { albums, query } }) {
      commit('requestGetAlbums');

      if (albums.next) {
        const offset = albums.offset + albums.limit;

        await api.search(query, ['album'], { offset }).then(
          data => {
            commit('requestGetAlbumsSuccess', data);
          },
          err => {
            commit('requestGetAlbumsError', err);
          }
        );
      }
    },
    async getArtists({ commit, state: { artists, query } }) {
      commit('requestGetArtists');

      if (artists.next) {
        const offset = artists.offset + artists.limit;

        await api.search(query, ['artist'], { offset }).then(
          data => {
            commit('requestGetArtistsSuccess', data);
          },
          err => {
            commit('requestGetArtistsError', err);
          }
        );
      }
    },
    async getPlaylists({ commit, state: { playlists, query } }) {
      commit('requestGetPlaylists');

      if (playlists.next) {
        const offset = playlists.offset + playlists.limit;

        await api.search(query, ['playlist'], { offset }).then(
          data => {
            commit('requestGetPlaylistsSuccess', data);
          },
          err => {
            commit('requestGetPlaylistsError', err);
          }
        );
      }
    },
    async getTracks({ commit, state: { tracks, query } }) {
      commit('requestGetTracks');

      if (tracks.next) {
        const offset = tracks.offset + tracks.limit;

        await api.search(query, ['track'], { offset }).then(
          data => {
            commit('requestGetTracksSuccess', data);
          },
          err => {
            commit('requestGetTracksError', err);
          }
        );
      }
    }
  },
  mutations: {
    // search
    setSearchQuery(state, payload) {
      state.query = payload;
    },
    requestSearch(state) {
      state.isLoading = true;
    },
    requestSearchError(state, payload) {
      state.isLoading = false;
      state.error = payload;
    },
    requestSearchSuccess(state, payload) {
      state.result = payload;
      state.albums = payload.albums;
      state.artists = payload.artists;
      state.playlists = payload.playlists;
      state.tracks = payload.tracks;
      state.isLoading = false;
    },
    // search albums
    requestGetAlbums(state) {
      state.albumsIsLoading = true;
    },
    requestGetAlbumsSuccess(state, payload) {
      state.albumsIsLoading = false;
      state.albums = {
        ...payload.albums,
        items: [...state.albums.items, ...payload.albums.items]
      };
    },
    requestGetAlbumsError(state, payload) {
      state.albumsIsLoading = false;
      state.albumsError = payload;
    },
    // search artists
    requestGetArtists(state) {
      state.artistsIsLoading = true;
    },
    requestGetArtistsSuccess(state, payload) {
      state.artistsIsLoading = false;
      state.artists = {
        ...payload.artists,
        items: [...state.artists.items, ...payload.artists.items]
      };
    },
    requestGetArtistsError(state, payload) {
      state.artistsIsLoading = false;
      state.artistsError = payload;
    },
    // search playlists
    requestGetPlaylists(state) {
      state.playlistsIsLoading = true;
    },
    requestGetPlaylistsSuccess(state, payload) {
      state.playlistsIsLoading = false;
      state.playlists = {
        ...payload.playlists,
        items: [...state.playlists.items, ...payload.playlists.items]
      };
    },
    requestGetPlaylistsError(state, payload) {
      state.playlistsIsLoading = false;
      state.playlistsError = payload;
    },
    requestGetTracks(state) {
      state.tracksIsLoading = true;
    },
    requestGetTracksSuccess(state, payload) {
      state.tracksIsLoading = false;
      state.tracks = {
        ...payload.tracks,
        items: [...state.tracks.items, ...payload.tracks.items]
      };
    },
    requestGetTracksError(state, payload) {
      state.tracksIsLoading = false;
      state.tracksError = payload;
    }
  }
};
