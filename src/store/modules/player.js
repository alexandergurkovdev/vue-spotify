import spotifyApi from '../../api';
import router from '@/router';
import Vue from 'vue';

export default {
  namespaced: true,
  state: {
    device_id: '',
    playback: '',
    playbackContext: ''
  },
  actions: {
    init({ commit, rootState, dispatch }) {
      const token = rootState.auth.token;
      async function waitForSpotifyWebPlaybackSDKToLoad() {
        return new Promise(resolve => {
          if (window.Spotify) {
            resolve(window.Spotify);
          } else {
            window.onSpotifyWebPlaybackSDKReady = () => {
              resolve(window.Spotify);
            };
          }
        });
      }

      async function initiatePlayer() {
        const { Player } = await waitForSpotifyWebPlaybackSDKToLoad();
        const sdk = new Player({
          name: 'Web Playback SDK',
          volume: 1.0,
          getOAuthToken: callback => {
            callback(token);
          }
        });
        // Error handling
        sdk.addListener('initialization_error', ({ message }) => {
          Vue.toasted.show(`${message}`, {
            type: 'error'
          });
        });
        sdk.addListener('authentication_error', ({ message }) => {
          Vue.toasted.show(`${message}`, {
            type: 'error'
          });
          rootState.auth.token = null;
          Vue.$cookies.remove('spotifyToken');
          router.push('/login');
        });
        sdk.addListener('account_error', ({ message }) => {
          Vue.toasted.show(`${message}`, {
            type: 'error'
          });
        });
        sdk.addListener('playback_error', ({ message }) => {
          Vue.toasted.show(`${message}`, {
            type: 'error'
          });
        });
        // Playback status updates
        sdk.addListener('player_state_changed', data => {
          commit('setPlaybackContext', data);
          dispatch('getCurrentPlaybak');
        });
        // Ready
        sdk.addListener('ready', ({ device_id }) => {
          commit('setDeviceId', device_id);

          spotifyApi.transferMyPlayback([device_id]);
        });
        // Not Ready
        sdk.addListener('not_ready', ({ device_id }) => {
          console.log('Not ready with device Id: ', device_id);
        });
        sdk.connect();
      }

      initiatePlayer();
    },
    getCurrentPlaybak({ commit }) {
      spotifyApi.getMyCurrentPlaybackState().then(
        data => {
          commit('setCurrentPlaybak', data);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setDeviceId(state, payload) {
      state.device_id = payload;
    },
    setPlaybackContext(state, payload) {
      state.playbackContext = payload;
    },
    setCurrentPlaybak(state, payload) {
      state.playback = payload;
    }
  },
  getters: {
    device_id(state) {
      return state.device_id;
    },
    playback(state) {
      return state.playback;
    },
    playbackContext(state) {
      return state.playbackContext;
    },
    isPlaying(state) {
      return state.playback.is_playing;
    }
  }
};
