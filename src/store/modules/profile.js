import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    profile: ''
  },
  actions: {
    async getMe({ commit, rootState }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getMe().then(
        data => {
          commit('setProfile', data);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setProfile(state, payload) {
      state.profile = payload;
    }
  },
  getters: {
    profile(state) {
      return state.profile;
    }
  }
};
