import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    items: [],
    playlists: [],
    limit: 16,
    offset: 0,
    total: 1,
    isMore: false,
    categoryID: null
  },
  actions: {
    async getGenres({ commit, rootState }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getCategories().then(
        data => {
          commit('setItems', data.categories.items);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    },
    async getGenrePlaylist({ commit, getters, rootState }, id) {
      if (getters.total > getters.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);

        await spotifyApi.getCategoryPlaylists(id, { offset: getters.offset, limit: getters.limit }).then(
          data => {
            commit('setOffset', data.playlists.offset + getters.limit);
            commit('setTotal', data.playlists.total);
            let items = [...data.playlists.items];
            items.forEach(item => {
              commit('setPlaylists', item);
            });
            commit('setIsMore', false);
          },
          err => {
            Vue.toasted.show(`${err}`, {
              type: 'error'
            });
          }
        );
      }
    }
  },
  mutations: {
    setItems(state, payload) {
      state.items = payload;
    },
    setPlaylists(state, payload) {
      state.playlists = [...state.playlists, payload];
    },
    setOffset(state, payload) {
      state.offset = payload;
    },
    setTotal(state, payload) {
      state.total = payload;
    },
    setIsMore(state, payload) {
      state.isMore = payload;
    },
    initData(state) {
      state.limit = 16;
      state.offset = 0;
      state.total = 1;
      state.playlists = [];
    },
    setCategoryId(state, payload) {
      state.categoryID = payload;
    }
  },
  getters: {
    items(state) {
      return state.items;
    },
    playlists(state) {
      return state.playlists;
    },
    isMore(state) {
      return state.isMore;
    },
    total(state) {
      return state.total;
    },
    limit(state) {
      return state.limit;
    },
    offset(state) {
      return state.offset;
    },
    categoryID(state) {
      return state.categoryID;
    }
  }
};
