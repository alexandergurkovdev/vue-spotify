import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    myTopArtists: {
      offset: 0,
      limit: 16,
      total: 1,
      items: [],
      isMore: false
    }
  },
  actions: {
    async getMyTopArtists({ commit, getters, rootState }) {
      if (getters.myTopArtists.total > getters.myTopArtists.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);
        await spotifyApi
          .getMyTopArtists({ offset: getters.myTopArtists.offset, limit: getters.myTopArtists.limit })
          .then(
            data => {
              commit('setOffset', data.offset + getters.myTopArtists.limit);
              commit('setTotal', data.total);
              let items = [...data.items];
              items.forEach(item => {
                commit('setItems', item);
              });
              commit('setIsMore', false);
            },
            err => {
              Vue.toasted.show(`${err}`, {
                type: 'error'
              });
            }
          );
      }
    }
  },
  mutations: {
    setItems(state, payload) {
      state.myTopArtists.items = [...state.myTopArtists.items, payload];
    },
    setOffset(state, payload) {
      state.myTopArtists.offset = payload;
    },
    setTotal(state, payload) {
      state.myTopArtists.total = payload;
    },
    setIsMore(state, payload) {
      state.myTopArtists.isMore = payload;
    }
  },
  getters: {
    myTopArtists(state) {
      return state.myTopArtists;
    }
  }
};
