import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    artistID: '',
    artist: '',
    tracks: [],
    albums: {
      limit: 25,
      offset: 0,
      items: []
    }
  },
  actions: {
    async getArtist({ commit, rootState, getters }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getArtist(getters.artistID).then(
        data => {
          commit('setArtist', data);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    },
    async getArtistAlbums({ commit, rootState, getters }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi
        .getArtistAlbums(getters.artistID, 'album,single', getters.albums.offset, getters.albums.limit)
        .then(
          data => {
            let items = [...data.items];
            items.forEach(item => {
              commit('setItems', item);
            });
          },
          err => {
            Vue.toasted.show(`${err}`, {
              type: 'error'
            });
          }
        );
    },
    async getArtistTopTracks({ getters, commit, rootState }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      spotifyApi.getArtistTopTracks(getters.artistID, 'RU').then(
        data => {
          commit('setTracks', data.tracks);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setTracks(state, payload) {
      state.tracks = payload;
    },
    initData(state) {
      state.albums = {
        limit: 25,
        offset: 0,
        items: []
      };
    },
    setItems(state, payload) {
      state.albums.items = [...state.albums.items, payload];
    },
    setArtist(state, payload) {
      state.artist = payload;
    },
    setArtistId(state, payload) {
      state.artistID = payload;
    }
  },
  getters: {
    albums(state) {
      return state.albums;
    },
    artist(state) {
      return state.artist;
    },
    artistID(state) {
      return state.artistID;
    },
    tracks(state) {
      return state.tracks;
    }
  }
};
