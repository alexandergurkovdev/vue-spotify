import spotifyApi from '../../api';
import Vue from 'vue';

export default {
  namespaced: true,
  state: {
    albums: {
      offset: 0,
      limit: 16,
      total: 1,
      items: [],
      isMore: false
    },
    tracks: [],
    album: null,
    albumID: null
  },
  actions: {
    async getMyAlbums({ commit, getters, rootState }) {
      if (getters.albums.total > getters.albums.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);
        await spotifyApi.getMySavedAlbums({ offset: getters.albums.offset, limit: getters.albums.limit }).then(
          data => {
            commit('setOffset', data.offset + getters.albums.limit);
            commit('setTotal', data.total);
            let items = [...data.items];
            items.forEach(item => {
              commit('setItems', item);
            });
            commit('setIsMore', false);
          },
          err => {
            Vue.toasted.show(`${err}`, {
              type: 'error'
            });
          }
        );
      }
    },
    async getMyChandedAlbums({ state, rootState }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getMySavedAlbums().then(
        data => {
          state.albums.items = [...data.items];
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    },
    async getAlbum({ rootState, getters, commit }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getAlbum(getters.albumID).then(
        data => {
          commit('setAlbum', data);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    },
    async getAlbumTracks({ rootState, getters, commit }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getAlbumTracks(getters.albumID).then(
        data => {
          commit('setTracks', data.items);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setItems(state, payload) {
      state.albums.items = [...state.albums.items, payload];
    },
    setOffset(state, payload) {
      state.albums.offset = payload;
    },
    setTotal(state, payload) {
      state.albums.total = payload;
    },
    setIsMore(state, payload) {
      state.albums.isMore = payload;
    },
    setAlbum(state, payload) {
      state.album = payload;
    },
    setAlbumId(state, payload) {
      state.albumID = payload;
    },
    setTracks(state, payload) {
      state.tracks = payload;
    }
  },
  getters: {
    albums(state) {
      return state.albums;
    },
    tracks(state) {
      return state.tracks;
    },
    album(state) {
      return state.album;
    },
    albumID(state) {
      return state.albumID;
    }
  }
};
