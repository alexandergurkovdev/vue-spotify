import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    playlist: '',
    myPlaylists: [],
    userID: null,
    playlistID: null,
    tracks: {
      limit: 16,
      offset: 0,
      total: 1,
      items: []
    },
    isMore: false
  },
  actions: {
    async getMyPlaylists({ commit }) {
      await spotifyApi.getUserPlaylists().then(
        data => {
          commit('setMyPlaylists', data.items);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    },
    async getPlaylistTracks({ getters, commit, rootState }) {
      if (getters.tracks.total > getters.tracks.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);
        await spotifyApi
          .getPlaylistTracks(getters.playlistID, {
            offset: getters.tracks.offset,
            limit: getters.tracks.limit
          })
          .then(
            data => {
              commit('setOffset', data.offset + getters.tracks.limit);
              commit('setTotal', data.total);
              let items = [...data.items];
              items.forEach(item => {
                commit('setItems', item);
              });
              commit('setIsMore', false);
            },
            err => {
              Vue.toasted.show(`${err}`, {
                type: 'error'
              });
            }
          );
      }
    },
    async fetchPlaylist({ commit, getters, rootState }) {
      const fields = 'uri, name, type, images, description, owner, followers';
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getPlaylist(getters.playlistID, getters.userID, fields).then(
        data => {
          commit('setPlaylist', data);
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setMyPlaylists(state, payload) {
      state.myPlaylists = payload;
    },
    setItems(state, payload) {
      state.tracks.items = [...state.tracks.items, payload];
    },
    initData(state) {
      state.tracks = {
        limit: 16,
        offset: 0,
        total: 1,
        items: []
      };
    },
    setIsMore(state, payload) {
      state.isMore = payload;
    },
    setPlaylist(state, payload) {
      state.playlist = payload;
    },
    setPlaylistId(state, payload) {
      state.playlistID = payload;
    },
    setUserId(state, payload) {
      state.userID = payload;
    },
    setOffset(state, payload) {
      state.tracks.offset = payload;
    },
    setTotal(state, payload) {
      state.tracks.total = payload;
    }
  },
  getters: {
    myPlaylists(state) {
      return state.myPlaylists;
    },
    playlist(state) {
      return state.playlist;
    },
    tracks(state) {
      return state.tracks;
    },
    playlistID(state) {
      return state.playlistID;
    },
    userID(state) {
      return state.userID;
    },
    isMore(state) {
      return state.isMore;
    }
  }
};
