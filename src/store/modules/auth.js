export default {
  namespaced: true,
  state: {
    token: null
  },
  mutations: {
    setToken(state, payload) {
      state.token = payload;
    }
  },
  getters: {
    token(state) {
      return state.token;
    }
  }
};
