import spotifyApi from '../../api';
import Vue from 'vue';
import { getParameterByName } from '@/utils';

export default {
  namespaced: true,
  state: {
    myArtists: {
      offset: 0,
      limit: 1,
      total: 1,
      items: [],
      isMore: false,
      after: null
    }
  },
  actions: {
    async getMyArtists({ commit, getters, rootState }) {
      if (getters.myArtists.total > getters.myArtists.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);
        await spotifyApi.getFollowedArtists(getters.myArtists.offset, getters.myArtists.after).then(
          data => {
            commit('setAfter', getParameterByName('after', data.artists.next));
            commit('setTotal', data.artists.total);
            let items = [...data.artists.items];
            items.forEach(item => {
              commit('setItems', item);
            });
            commit('setOffset', getters.myArtists.items.length);
            commit('setIsMore', false);
          },
          err => {
            Vue.toasted.show(`${err}`, {
              type: 'error'
            });
          }
        );
      }
    },
    async getMyChangedArtists({ state, rootState }) {
      spotifyApi.setAccessToken(rootState.auth.token);
      await spotifyApi.getFollowedArtists().then(
        data => {
          state.myArtists.items = [...data.artists.items];
        },
        err => {
          Vue.toasted.show(`${err}`, {
            type: 'error'
          });
        }
      );
    }
  },
  mutations: {
    setItems(state, payload) {
      state.myArtists.items = [...state.myArtists.items, payload];
    },
    setOffset(state, payload) {
      state.myArtists.offset = payload;
    },
    setTotal(state, payload) {
      state.myArtists.total = payload;
    },
    setIsMore(state, payload) {
      state.myArtists.isMore = payload;
    },
    setAfter(state, payload) {
      state.myArtists.after = payload;
    }
  },
  getters: {
    myArtists(state) {
      return state.myArtists;
    }
  }
};
