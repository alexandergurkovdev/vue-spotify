import Vue from 'vue';
import spotifyApi from '../../api';

export default {
  namespaced: true,
  state: {
    newReleases: {
      offset: 0,
      limit: 16,
      total: 1,
      items: [],
      isMore: false
    }
  },
  actions: {
    async getNewReleases({ commit, getters, rootState }) {
      if (getters.newReleases.total > getters.newReleases.offset) {
        spotifyApi.setAccessToken(rootState.auth.token);
        await spotifyApi.getNewReleases({ offset: getters.newReleases.offset, limit: getters.newReleases.limit }).then(
          data => {
            commit('setOffset', data.albums.offset + getters.newReleases.limit);
            commit('setTotal', data.albums.total);
            let items = [...data.albums.items];
            items.forEach(item => {
              commit('setItems', item);
            });
            commit('setIsMore', false);
          },
          err => {
            Vue.toasted.show(`${err}`, {
              type: 'error'
            });
          }
        );
      }
    }
  },
  mutations: {
    setItems(state, payload) {
      state.newReleases.items = [...state.newReleases.items, payload];
    },
    setOffset(state, payload) {
      state.newReleases.offset = payload;
    },
    setTotal(state, payload) {
      state.newReleases.total = payload;
    },
    setIsMore(state, payload) {
      state.newReleases.isMore = payload;
    }
  },
  getters: {
    newReleases(state) {
      return state.newReleases;
    }
  }
};
